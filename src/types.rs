use std::cmp::Ordering;
use std::{env, fs, process};

use serde::Deserialize;
use thiserror::Error;

const CONFIG_PATH: &str = "/home/sohun/.config/weather/config.toml";

// Represents user configs
#[derive(Debug, Deserialize, Clone)]
pub struct Configuration {
    pub api_key: String,
    pub city_id: String,
    pub display: String,
}

impl Configuration {
    pub fn new() -> Result<Self, Error> {
        let content = dirs::config_dir()
            .and_then(|mut path| {
                path.push("weather/config.toml");
                fs::read_to_string(&path).ok()
            })
            .or_else(|| {
                let mut dir = env::current_exe().ok().unwrap();
                dir.pop();
                dir.push("config.toml");
                fs::read_to_string(&dir).ok()
            })
            .ok_or(Error::MissingConfigFile)?;

        let decoded: Configuration = toml::from_str(&content)?;
        Ok(decoded)
    }
}

// Supported temperature units
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Unit {
    Kelvin,
    Celcius,
    Fahrenheit,
}

impl Unit {
    pub fn to_api(&self) -> &str {
        match self {
            Unit::Kelvin => "kelvin",
            Unit::Celcius => "metric",
            Unit::Fahrenheit => "imperial",
        }
    }
}

// Wrapper type for a temperature, allows abstractions around units, conversions ...
#[derive(Debug, Clone, Copy, Eq)]
pub struct Temperature(pub i16, pub Unit);

impl PartialEq for Temperature {
    fn eq(&self, other: &Temperature) -> bool {
        let other = other.as_unit(self.1);
        self.0 == self.0
    }
}

impl PartialOrd for Temperature {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

impl Ord for Temperature {
    fn cmp(&self, other: &Self) -> Ordering {
        let other = other.as_unit(self.1);
        self.0.cmp(&other.0)
    }
}

impl Temperature {
    pub fn as_unit(self, unit: Unit) -> Temperature {
        use Unit::*;
        match (self.0, self.1, unit) {
            // Kelvin to Celcius
            (val, Kelvin, Celcius) => Temperature(val - 273.15 as i16, Celcius),
            // Kelvin to Fahrenhet
            (val, Kelvin, Fahrenheit) => Temperature(val * 9 / 5 - 459.67 as i16, Fahrenheit),
            // Celcius to Kelvin
            (val, Celcius, Kelvin) => Temperature(val + 273.15 as i16, Kelvin),
            // Celcius to Fahrenheit
            (val, Celcius, Fahrenheit) => Temperature(val * 9 / 5 + 32 as i16, Fahrenheit),
            // Fahrenheit to Kelvin
            (val, Fahrenheit, Kelvin) => Temperature((val + 459.67 as i16) * 5 / 9 as i16, Kelvin),
            // Fahrenheit to Celcius
            (val, Fahrenheit, Celcius) => Temperature((val - 32) * 5 / 9 as i16, Celcius),
            // Identity
            _ => self,
        }
    }
}

fn api_key_from_env() -> String {
    match env::var("OWM_API_KEY") {
        Ok(key) => key,
        Err(_) => {
            eprintln!("\nCould not find Open Weather Map API key. Make sure api_key is set in the config file, or the OWM_API_KEY env variable is defined.");
            process::exit(1);
        }
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("Config file error, please check {}: {}", CONFIG_PATH, _0)]
    InvalidConfigFile(#[from] toml::de::Error),

    #[error("F=Could not open config file, please check {}", CONFIG_PATH)]
    MissingConfigFile,

    #[error("Invalid response from Open Weather Map")]
    InvalidResponse,
}
