use owm_rs::prelude::*;

const CITY: &str = "Kitchener";
const OWM_API_KEY: &str = "7b4d0758cc883f6d007491edb55a7a27";

pub fn kelvin_to_celcius(temp: f32) -> f32 {
    return temp - 273.15;
}

pub fn get_icon(code: &str) -> char {
    match code {
        "01d" => '',
        "01n" => '',
        "02d" => '',
        "02n" => '',
        "03d" | "03n" => '',
        "04d" | "04n" => '',
        "09d" => '',
        "09n" => '',
        "10d" => '',
        "10n" => '',
        "11d" => '',
        "11n" => '',
        "13d" => '',
        "13n" => '',
        "50d" => '',
        "50n" => '',
        _ => '',
    }
}

#[tokio::main]
async fn main() {
    let coords_result = get_city_coordinates(String::from(CITY), String::from(OWM_API_KEY)).await;
    let coordinates = match coords_result {
        Ok(ok) => ok,
        Err(err) => {
            println!("Error trying to retrieve coordinates: {}", err);
            std::process::exit(1);
        }
    };

    let weather_result = get_weather_by_coordinates(
        coordinates.get_latitude(),
        coordinates.get_longitude(),
        String::from(OWM_API_KEY),
    )
    .await;

    let weather = match weather_result {
        Ok(ok) => ok,
        Err(err) => {
            println!("Error trying to retrieve the weather: {}", err);
            std::process::exit(1);
        }
    };

    let temp: f32 = weather.main.temp;
    let temp_c: f32 = kelvin_to_celcius(temp);
    println!(
        "{}  {:.0}°C",
        get_icon(weather.weather[0].icon.as_str()),
        temp_c
    );
}
